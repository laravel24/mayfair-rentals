<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OriginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('origins')->insert([
            'name' => 'Asia',
        ]);

        DB::table('origins')->insert([
            'name' => 'Europe',
        ]);

    }
}