<?php

use Illuminate\Database\Seeder;

class LabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('labels')->insert([
            'name' => 'Regular',
        ]);

        DB::table('labels')->insert([
            'name' => 'New Arrival',
        ]);

        DB::table('labels')->insert([
            'name' => 'Best Reviewed',
        ]);

        DB::table('labels')->insert([
            'name' => 'Mayfair Exclusive',
        ]);

        DB::table('labels')->insert([
            'name' => 'Vintage',
        ]);

        DB::table('labels')->insert([
            'name' => 'Limited',
        ]);

    }
}