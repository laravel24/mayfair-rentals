<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => 'Rozelyn-Claire',
            'lastname' => 'Ferrer',
            'email' => 'rcferrer@admin.mayfair.com',
            'password' => Hash::make('admin'),
            'role_id' => 1,
            'image' => '/storage/users/roze.jpg',
        ]);

        DB::table('users')->insert([
            'firstname' => 'Bill',
            'lastname' => 'Gates',
            'email' => 'bgates@email.com',
            'password' => Hash::make('billgates'),
            'role_id' => 2,
            'image' => '/storage/users/bill.jpg',
        ]);

        DB::table('users')->insert([
            'firstname' => 'Steve',
            'lastname' => 'Jobs',
            'email' => 'sjobs@email.com',
            'password' => Hash::make('stevejobs'),
            'role_id' => 2,
            'image' => '/storage/users/steve.jpg',
        ]);
    }
}