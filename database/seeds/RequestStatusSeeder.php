<?php

use Illuminate\Database\Seeder;

class RequestStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('request_statuses')->insert([
            'name' => 'Pending',
        ]);

        DB::table('request_statuses')->insert([
            'name' => 'Rejected',
        ]);

        DB::table('request_statuses')->insert([
            'name' => 'Approved',
        ]);

        DB::table('request_statuses')->insert([
            'name' => 'Completed',
        ]);
    }
}