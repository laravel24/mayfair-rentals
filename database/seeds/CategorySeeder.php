<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Clothing',
        ]);

        DB::table('categories')->insert([
            'name' => 'Jewelry',
        ]);

        DB::table('categories')->insert([
            'name' => 'Props',
        ]);
    }
}