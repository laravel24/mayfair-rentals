<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PaymentModeSeeder::class,
            StatusSeeder::class,
            CategorySeeder::class,
            LabelSeeder::class,
            OriginSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            ProductStatusSeeder::class,
            RequestStatusSeeder::class,
        ]);
    }
}