<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestedProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requested_product', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_request_id');
            $table->unsignedBigInteger('product_id');
            $table->float('price');
            $table->timestamps();

            $table->foreign('user_request_id')->references('id')->on('user_requests');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requested_product');
    }
}