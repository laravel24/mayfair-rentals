@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Deleted Types</h1>

            <hr>

            <div class="d-flex flex-wrap">
                @foreach ($types as $type)
                    @include('types.partials.type_card')
                @endforeach
            </div>

            <div class="d-flex justify-content-center mt-5">
                <a href="{{ route('types.index') }}" class="btn btn-outline-dark">Back to current types</a>
            </div>
        </div>
    </div>
</div>

@endsection