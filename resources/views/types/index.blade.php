@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">

            @includeWhen(Session::has('success'), 'partials.success_message')
            @includeWhen($errors->any(), 'partials.error_message')

            <h1>Create new product type</h1>
            
            <form action="{{ route('types.store') }}" method="POST">
                @csrf
                <div class="input-group mb-3">
                    <input type="text" name="name" class="form-control" placeholder="Input product type here" aria-label="Recipient's username" aria-describedby="button-addon2">
                    <select name="category_id" class="btn btn-yellow" style="border-radius: 0px">
                        @foreach ($categories as $category)
                        <option class="dropdown-item" value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-success" type="button" id="create-category">Create new product type</button>
                    </div>
                </div>
            </form>

            <hr>

            <h1>Current product types</h1>
            <div class="d-flex">
                <form action="{{ route('types.index') }}" class="col-12 d-flex p-0">
                    <div class="mr-auto">
                        <a href="{{ route('types.index') }}" name="category_id" class="btn btn-outline-dark mr-1" >All</a>
                        @foreach ($categories as $category)
                            <button name="category_id" class="btn mr-1 
                            @if($category->id === 1)
                                btn-danger badge-c
                            @elseif($category->id === 2)
                                btn-warning badge-j
                            @else
                                btn-info badge-p
                            @endif
                            " value={{ $category->id }}>{{ $category->name }}</button>
                        @endforeach

                    </div>

                    <div class="input-group ml-auto w-50">
                        <input name="search_category" type="text" class="form-control" placeholder="Search product type">
                        <div class="input-group-append">
                        <button class="btn btn-primary" value="B" id="button-addon2">Search</button>
                        </div>
                    </div>
                </form>
            </div>

            <hr>
            
            @if (Session::has('empty_search'))
                @include('partials.empty_search_message')
            @else
                <div class="d-flex flex-wrap">
                    @foreach ($types as $type)
                        @include('types.partials.type_card')
                    @endforeach
                </div>
            @endif

            <div class="d-flex justify-content-center mt-5">
                <a href="{{ route('types.deleted') }}" class="btn btn-outline-dark">See deleted types</a>
            </div>

        </div>

    </div>
</div>
@endsection
