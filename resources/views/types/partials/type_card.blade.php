<div class="col-3 col-lg-3 col-md-6 col-sm-12">
    <div class="card border-dark mb-4" style="max-width: 18rem">
        <div class="card-header d-flex">
            <h5 class="m-0">{{ $type->name }}</h5>
            <p class="m-0 ml-auto"><span class="badge 
                @if($type->category->id == 1)
                badge-danger badge-c
                @elseif($type->category->id == 2)
                badge-warning badge-j
                @else
                badge-info badge-p
                @endif
                ">{{ $type->category->name }}</span></p>
        </div>
    
        <div class="card-body text-dark">
        <p class="card-text">Available Products: 
            {{ $products->where('type_id', $type->id)->where('product_status_id', 1)->count() }}/{{ $products->where('type_id', $type->id)->count() }}
        </p>

        @if($type->trashed())
        <div class="d-flex justify-content-center">
            <form action="{{ route('types.restore', ['type' => $type->id]) }}" method="post">
                @csrf
                @method('PUT')
            <button class="btn btn-outline-dark">Restore</button>
            </form>
        </div>
        @else
        <div class="d-flex flex-wrap">
            <a href="{{ route('types.show', ['type' => $type]) }}" class="btn btn-secondary">Show</a>

            <button class="btn btn-secondary mx-1" data-toggle="modal" data-target="#edit-types{{ $type->id }}">Edit</button>

            <form action="{{ route('types.destroy', ['type' => $type->id]) }}" method="POST">
                @csrf
                @method('DELETE')
                <button class="btn btn-secondary">Delete</button>
            </form>
        </div>
        @endif
        

        </div>
    </div>
</div>

  
  <!-- Modal -->
<div class="modal fade" id="edit-types{{ $type->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit {{ $type->name }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            
            <form action="{{ route('types.update', ['type' => $type->id]) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label for="name">Type Name</label>
                        <input type="text" name="name" class="form-control" value="{{ $type->name }}">
                    </div>

                    <div class="form-group mb-3">
                        <label for="category_id">Category</label>
                        <select class="btn btn-outline-primary mx-1" name="category_id">
                            @foreach ($categories as $category)
                            <option class="dropdown-item" value="{{ $category->id }}"
                                {{ $category->id === $type->category_id ? "selected" : "" }} 
                            >{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning edit-types" data-id="{{ $type->id }}">Edit type</button>
                </div>
            </form>

        </div>
    </div>
</div>