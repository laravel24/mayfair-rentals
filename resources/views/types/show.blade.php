@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @foreach ($products as $product)
            <div class="col-4">
                @include('products.partials.product_card')
            </div>
            @endforeach
        </div>
    </div>
@endsection