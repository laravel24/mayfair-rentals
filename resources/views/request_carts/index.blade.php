@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 flex-column py-3">
                @includeWhen(Session::has('request_cart'), 'request_carts.partials.request_form_table')
                @if (Session::get('request_cart') === null)
                    @include('request_carts.partials.empty_request_form')
                @endif
            </div>
        </div>
    </div>
@endsection