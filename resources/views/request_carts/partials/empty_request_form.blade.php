<div class="alert alert-secondary text-center p-2" role="alert">
    <p class="mb-0">You have no items in your request form.</p>
    <p class="mb-0"><a href="{{ route('products.index') }}">Check our products</a></p>
</div>