<h1 class="text-center mb-3">Request Form</h1>
<div class="d-flex justify-content-center">
    <div class="col-8">
        <div>
            <table class="table mb-3">
                <thead class="thead">
                    <tr>
                        <th scope="col">Category</th>
                        <th scope="col">Product Name</th>
                        <th scope="col">Status</th>
                        <th scope="col">Base Price</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                        @foreach ($products as $product)
                        <tr>
                            <td >
                                <span class="badge 
                                @if($product->type->category_id === 1)
                                badge-danger
                                @elseif($product->type->category_id === 2)
                                badge-warning
                                @else
                                badge-info
                                @endif
                                ">
                                    {{ $product->type->category->name }}
                                </span>
                            </td>
                            <td>
                                {{ $product->name }}
                            </td>
                            <td>
                                <span class="badge 
                                @if($product->product_status->id === 1)
                                badge-success
                                @else
                                badge-danger
                                @endif
                                ">
                                    {{ $product->product_status->name }}
                                </span>
                            </td>
                            <td>&#8369; {{ number_format($product->price, 2) }}</td>
                            <td>
                                <form action="{{ route('request_carts.destroy', ['request_cart' => $product->id]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
    
                                    <button class="btn btn-danger">Remove</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" class="text-right"><strong>Subtotal</strong></td>
                            <td colspan="2">&#8369; {{ number_format($subtotal, 2) }}</td>
                        </tr>
                </tbody>
            </table>    
        </div>

        <hr>

        <form action="{{ route('user_requests.store') }}" method="POST">
            @csrf
            
            <div class="d-flex justify-content-center mb-5">
                <div class="form-group col-6 mr-2">
                    <label for="borrow_date">Borrow Date:</label>
                    <input required="" type="date" id="borrow_date" name="borrow_date" class="form-control">
                </div>
            
                <div class="form-group col-6 ml-2">
                    <label for="borrow_date">Return Date:</label>
                    <input required="" type="date" id="return_date" name="return_date" class="form-control">
                </div>
            </div>

            <div>
                <p class="text-muted text-center"><em>Once request has been submitted, it may take 2-8 working hours to process your request.</em></p>
        
            </div>
        
            <div class="d-flex justify-content-center">
                <button class="btn btn-success btn-lg">Submit Request</button>
            </div>
        </form>


        <div class="mt-5">
            <a href="{{ route('products.index') }}" class="btn btn-warning">Request More Items</a>
        </div>
        
        <div class="mt-5">
            <form action="{{ route('request_carts.clear') }}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger">Clear Request Form</button>
            </form>
        </div>

    </div>
    
</div>