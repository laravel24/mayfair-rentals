@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">

                @includeWhen($errors->any(),'partials.error_message')

                <h1>Create Transaction</h1>

                <hr>
                <form action="{{ route('transactions.store', ['user_request' => $user_request->id]) }}" method="post">
                    @csrf
                    {{-- <div class="form-group">
                        <label for="address">Name of Receiver</label>
                        <input type="text" name="name" class="form-control" value="{{ Auth::user()->firstname}} {{ Auth::user()->lastname }}">
                    </div> --}}

                    <div class="form-group">
                        <label for="address">Shipping Address</label>
                        <input required="" type="text" name="address" class="form-control" placeholder="Input your address here">
                    </div>

                    <button type="submit" class="btn btn-lg btn-success">Pay via COD</button>
                </form>

                <hr>

                <p>Request Information</p>
                <div class="col-12 mb-3 p-0">
                    @include('user_requests.partials.request_card')
                </div>

            </div>
        </div>
    </div>
@endsection