<div id="accordion">
    <div class="card">
      <div class="card-header" id="headingOne">
        
        <p class="mb-0">
            
          <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne{{ $transaction->id }}" aria-expanded="true" aria-controls="collapseOne{{ $transaction->id }}">

            @if ($transaction->status_id == 1)
            <span class="badge badge-warning mr-2"><i class="fa fa-truck"></i></span>
            @elseif($transaction->status_id == 2)
            <span class="badge badge-danger mr-2"><i class="fa fa-truck"></i></span>
            @elseif($transaction->status_id == 3)
            <span class="badge badge-success mr-2"><i class="fa fa-truck"></i></span>
            @else
            <span class="badge badge-secondary mr-2"><i class="fa fa-truck"></i></span>
            @endif

                TRANSACTION CODE: {{ $transaction->code }} 
                <em>({{ $transaction->created_at }} UTC)</em>
            
          </button>
        </p>
      </div>
    
      <div id="collapseOne{{ $transaction->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
        <div class="card-body">
            <p>Customer Name: {{ $transaction->user->firstname . " " . $transaction->user->lastname }}</p>
            <p>Request Code: {{ $transaction->code }}</p>
            <p>
                Date Created:
                {{ date("M d, Y h:i:sa", strtotime($transaction->created_at)) }}
            </p>

            <div class="d-flex align-items-center">
            <p class="mb-0">Status: 
                <span class="badge 
                @if($transaction->status_id === 1)
                badge-warning
                @elseif($transaction->status_id === 2)
                badge-danger
                @elseif($transaction->status_id === 3)
                badge-success
                @else
                badge-secondary
                @endif
                ">{{ $transaction->status->name }}</span>
            </p>
            @can('isAdmin')
                <div class="mx-1">
                    <button class="badge btn btn-outline-dark py-0" data-toggle="modal" data-target="#edit-request-status{{ $transaction->id }}">...</button>
                </div>
            @endcan
            </div>

            <hr>

            <p class="text-center"><strong>Transaction Details</strong></p>
            <p>
                Shipping address: 
                {{ $transaction->address }}
            </p>
            <p>
                Borrow Date:
                {{ date("M d, Y", strtotime($transaction->user_request->borrow_date)) }}
            </p>
            <p>
                Return Date:
                {{ date("M d, Y", strtotime($transaction->user_request->return_date)) }}
            </p>

            <hr>

            <p class="text-center"><strong>Product Details</strong></p>

            <table class="table">
                <thead class="thead">
                    <tr>
                        <th scope="col">Product Code</th>
                        <th scope="col">Product Name</th>
                        <th scope="col">Current Unit Status</th>
                        <th scope="col">Base Price per Unit</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transaction->user_request->products as $product)
                    <tr>
                        <td>{{ $product->code }}</td>
                        <td scope="row">{{ $product->name }}</td>
                        <td>
                            <span class="badge 
                            @if($product->product_status->id === 1)
                            badge-success
                            @else
                            badge-danger
                            @endif
                            ">
                                {{ $product->product_status->name }}
                            </span>
                        </td>
                        <td>&#8369; {{ number_format($product->price, 2) }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" class="text-right"><strong>Subtotal</strong></td>
                        <td>&#8369; {{ number_format(($transaction->user_request->total / date_diff(date_create($transaction->user_request->borrow_date), date_create($transaction->user_request->return_date))->format('%a')) - 50, 2) }} </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-right"><strong>Rent multiplier</strong></td>
                        <td>x {{ date_diff(date_create($transaction->user_request->borrow_date), date_create($transaction->user_request->return_date))->format('%a') }}</td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-right"><strong>Shipping Charge</strong></td>
                        <td>+ &#8369; 50</td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-right"><strong>TOTAL</strong></td>
                        <td><strong>&#8369; {{ number_format($transaction->total, 2) }}</strong></td>
                    </tr>
                </tbody>
            </table>

        </div>
      </div>
    </div>
      
</div>

<!-- Modal -->
<div class="modal fade" id="edit-request-status{{ $transaction->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Edit Request Status</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            <form action="{{ route('transactions.update', ['transaction' => $transaction->id])}}" method="POST">
            @csrf
            @method('PUT')
                <div class="row d-flex flex-wrap mx-auto">
                    @foreach ($statuses as $status)
                    <div class="input-group col-lg-6 col-md-6 px-1 mb-3 col-sm-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <input type="radio" name="status_id" value="{{ $status->id }}" aria-label="Radio button for following text input">
                            </span>
                            <span class="input-group-text bg-white" style="min-width: 10em">{{ $status->name }}</span>
                        </div>
                        <div class="input-group-append" aria-label="Amount (to the nearest dollar)">
                            <span class="input-group-text pr-0
                            @if($status->id == 1)
                                bg-warning border border-warning
                            @elseif($status->id == 2)
                                bg-info border border-info
                            @elseif($status->id == 3)
                                bg-success border border-success
                            @else
                                bg-danger border border-danger
                            @endif">
                            </span>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </form>
            </div>
        </div>
    </div>
</div>
