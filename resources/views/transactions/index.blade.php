@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-12">
            @includeWhen(Session::has('success'), 'partials.success_message')
            @includeWhen($errors->any(), 'partials.error_message')

            <h1>Transactions</h1>
            <hr>
            @foreach ($transactions as $transaction)
                @include('transactions.partials.transactions_table')
            @endforeach
        </div>
    </div>
</div>

@endsection