@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                @includeWhen(Session::has('success'), 'partials.success_message')
            </div>
            <div class="col-12 d-flex align-items-center">
                <div>
                    <h1>Requests</h1>
                </div>

                <div class="d-flex ml-auto">
                    <div class="dropdown">
                        <form action="{{ route('user_requests.index') }}">
                        <button class="btn btn-outline-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:10em">
                          Sort by
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a href="{{ route('user_requests.index') }}" class="dropdown-item">All</a>
                            @foreach ($request_statuses as $request_status)
                            <button type="submit" class="dropdown-item" name="request_status_id" value="{{ $request_status->id }}" {{ Request::query("request_status_id") == $request_status->id ? "Selected" : ""}}>{{ $request_status->name }}</button>
                            @endforeach
                        </div>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>

        <hr>
        
        <div class="row">
            <div class="col-12">
                @foreach ($user_requests as $user_request)
                    @include('user_requests.partials.request_card')
                @endforeach
            </div>
        </div>

    </div>
@endsection