@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 mb-2">
            @includeWhen($errors->any(), 'partials.error_message')
            @includeWhen(Session::has('success'), 'partials.success_message')

            <div class="d-flex flex-wrap align-items-center">
                <h1 class="mr-auto">Products List</h1>
                @can('isAdmin')
                <p><button class="btn btn-warning btn-lg ml-auto" data-toggle="modal" data-target="#add-product">Add New Product</button></p>
                @endcan
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-2">
            <div>
                <div class="list-group">
                    <li class="list-group-item list-group-item-action bg-dark text-light">
                      Types
                    </li>

                    @foreach ($types as $type)
                    <form action="{{ route('products.by_category', ['category' => $category]) }}">
                        <button type="submit" href="{{ route('products.by_category', ['category' => $category]) }}" name="type_id" value="{{ $type->id }}" class="rounded-0 border-top-0 list-group-item list-group-item-action">{{ $type->name }}</button>
                    </form>
                    @endforeach
                </div>

                <hr>

                <div class="list-group">
                    <li class="list-group-item list-group-item-action bg-dark text-light">
                      Label
                    </li>

                    @foreach ($labels as $label)
                    <form action="{{ route('products.by_category', ['category' => $category]) }}">
                        <button type="submit" href="{{ route('products.by_category', ['category' => $category]) }}" name="label_id" value="{{ $label->id }}" class="rounded-0 border-top-0 list-group-item list-group-item-action">{{ $label->name }}</button>
                    </form>
                    @endforeach
                </div>

                <hr>

                <div class="list-group">
                    <li class="list-group-item list-group-item-action bg-dark text-light">
                      Origins
                    </li>

                    @foreach ($origins as $origin)
                    <form action="{{ route('products.by_category', ['category' => $category]) }}">
                        <button type="submit" href="{{ route('products.by_category', ['category' => $category]) }}" name="origin_id" value="{{ $origin->id }}" class="rounded-0 border-top-0 list-group-item list-group-item-action">{{ $origin->name }}</button>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-10">
            <div class="d-flex flex-wrap">
                @foreach ($products as $product)
                    <div class="col-4 p-0">
                        @include('products.partials.product_card')
                    </div>
                @endforeach
            </div>

            @includeWhen($products->count() === 0, 'products.partials.no_items')
        </div>
    </div>

    <div class="d-flex justify-content-center mt-5">
        <a href="{{ route('products.deleted') }}" class="btn btn-outline-dark">See deleted products</a>
    </div>

</div>

<!-- modal -->
<div class="modal fade" id="add-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add new product</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label for="name">Product Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter product name">
                </div>

                <div class="form-group">
                    <label for="price">Product Price</label>
                    <input type="text" class="form-control" name="price" placeholder="Enter product price">
                </div>

                <div class="form-group">
                    <label for="image">Product Image</label>
                    <input type="file" class="form-control" name="image">
                </div>

                <div class="form-group">
                    <label for="description">Product Description</label>
                    <textarea class="form-control" name="description" placeholder="Enter product description" rows="3"></textarea>
                </div>

                <div class="form-group">
                    <label for="type_id">Product Type</label>
                    <select class="btn btn-outline-primary mx-1" name="type_id">
                        @foreach ($types as $type)
                        <option class="dropdown-item" value="{{ $type->id }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="origin_id">Product Origin</label>
                    <select class="btn btn-outline-primary mx-1" name="origin_id">
                        @foreach ($origins as $origin)
                        <option class="dropdown-item" name="origin_id" value="{{ $origin->id }}">{{ $origin->name }}</option>
                        @endforeach
                    </select>
                </div>  
                
                <div class="form-group">
                    <label for="product_status_id">Availability</label>
                    <select class="btn btn-outline-primary mx-1" name="product_status_id">
                        @foreach ($product_statuses as $product_status)
                        <option class="dropdown-item" name="product_status_id" value="{{ $product_status->id }}">{{ $product_status->name }}</option>
                        @endforeach
                    </select>
                </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
        </div>
      </div>
    </div>
</div>

@endsection