@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Deleted Products</h1>
                
                <hr>

                <div class="d-flex flex-wrap">
                    @foreach ($products as $product)
                        <div class="col-4">
                            @include('products.partials.product_card')
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-center mt-5">
            <a href="{{ route('products.index') }}" class="btn btn-outline-dark">Back to current products</a>
        </div>
    </div>
@endsection