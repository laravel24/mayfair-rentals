<div class="alert alert-secondary text-center" role="alert">
    <p class="mb-0">No items match your query.</p>
</div>