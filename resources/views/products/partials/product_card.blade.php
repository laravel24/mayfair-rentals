<div class="col-12">
    <div class="card border-dark mb-4" style="max-width: 18rem">
        <img src="{{ $product->image }}" class="card-img-top" style="height: 25em; object-fit: cover">
        <div class="card-body text-dark">
        <h5>{{ $product->name }}</h6>
        {{-- <div class="d-flex"> --}}
            <span class="badge 
            @if($product->category_id == 1 || $product->type->category_id == 1)
            badge-danger badge-c
            @elseif($product->category_id == 2 || $product->type->category_id == 2)
            badge-warning badge-j
            @else
            badge-info badge-p
            @endif
        ">{{ $product->type->category->name }}</span>
        {{-- </div> --}}
        <p class="badge badge-secondary">{{ $product->type->name }}</p>
        <p class="card-text">Base Price: &#8369; {{ number_format($product->price, 2) }}</p>

        @can('isAdmin')
        @if($product->trashed())
        <div>
            <form action="{{ route('products.restore', ['product' => $product->id]) }}" method="post">
                @csrf
                @method('PUT')
            <button class="btn btn-outline-dark w-100 mb-1">Restore</button>
            </form>
        </div>
        @else
        <div class="row">
            <div class="col-12 d-flex flex-wrap mb-2">
                <div class="col-6 pr-1 pl-0">
                    <button class="btn btn-secondary w-100" data-toggle="modal" data-target="#edit-products{{ $product->id }}">Edit</button>
                </div>
    
                <div class="col-6 pr-0 pl-1">
                    <form action="{{ route('products.destroy', ['product' => $product->id]) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-secondary w-100">Delete</button>
                    </form>
                </div>
            </div>
        </div>
        @endif
        @endcan
        @if ($product->trashed())
        @else
        <div class="mb-1">
            <a href="{{ route('products.show', ['product' => $product->id]) }}" class="btn btn-secondary w-100">Show</a>
        </div>
        @endif

        @cannot('isAdmin')
        <div class="my-1">
            <button class="btn btn-success my-1 w-100" data-toggle="modal" data-target="#request-item{{ $product->id }}">Request Item</button>
        </div>
        @endcannot

        </div>
    </div>
</div>

  
  <!-- Modal for edit -->
<div class="modal fade" id="edit-products{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit {{ $product->name }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            
            <form action="{{ route('products.update', ['product' => $product->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="modal-body">
                    <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
        
                        <div class="form-group">
                            <label for="name">Product Name</label>
                            <input type="text" value="{{ $product->name }}" class="form-control" name="name" placeholder="Enter product name">
                        </div>
        
                        <div class="form-group">
                            <label for="price">Product Price</label>
                            <input type="text" value="{{ $product->price }}" class="form-control" name="price" placeholder="Enter product price">
                        </div>
        
                        <div class="form-group">
                            <label for="image">Product Image</label>
                            <input type="file" value="{{ $product->image }} class="form-control" name="image">
                        </div>
        
                        <div class="form-group">
                            <label for="description">Product Description</label>
                            <textarea class="form-control" name="description" value="{{ $product->description }} rows="3">{{ $product->description }}</textarea>
                        </div>
        
                        <div class="form-group">
                            <label for="type_id">Product Type</label>
                            <select class="btn btn-outline-primary mx-1" name="type_id">
                                @foreach ($types as $type)
                                <option class="dropdown-item" value="{{ $type->id }}"
                                    {{ $product->type_id === $type->id ? "selected" : "" }} 
                                >{{ $type->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="origin_id">Product Origin</label>
                            <select class="btn btn-outline-primary mx-1" name="origin_id">
                                @foreach ($origins as $origin)
                                <option class="dropdown-item" name="origin_id" value="{{ $origin->id }}" 
                                    {{ $product->origin_id === $origin->id ? "selected" : "" }}
                                >{{ $origin->name }}</option>
                                @endforeach
                            </select>
                        </div>   
                        
                        <div class="form-group">
                            <label for="product_status_id">Availability</label>
                            <select class="btn btn-outline-primary mx-1" name="product_status_id">
                                @foreach ($product_statuses as $product_status)
                                <option class="dropdown-item" name="product_status_id" value="{{ $product_status->id }}">{{ $product_status->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <hr class="mt-0">
                        <div class="col-12 d-flex flex-wrap p-0">
                            @foreach($labels as $label)
                            <div class="input-group mb-3 col-6">
                                <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <input type="checkbox" name="label_id[]" value="{{ $label->id }}" aria-label="Checkbox for following text input">
                                </div>
                                </div>
                                <label for="label_id[]" class="form-control">{{ $label->name }}</label>
                            </div>
                            @endforeach
                        </div>
                </div>

                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning edit-products" data-id="{{ $product->id }}">Edit type</button>
                </div>
            </form>

        </div>
    </div>
</div>

<!-- Modal for request -->
<div class="modal fade" id="request-item{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Request {{ $product->name }}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <div class="d-flex">
                <img src="{{ $product->image }}" class="mr-3" alt="" style="height:8em">
                
                <div>
                    <p class="d-flex align-items-center">Product Name: {{ $product->name }}</p>
    
                    <p class="d-flex align-items-center">Product Code: {{ $product->code }}</p>

                    <p class="d-flex align-items-center">Base Rent Price: &#8369; {{ $product->price }}</p>
                </div>
            </div>

            <hr>

            <form action="{{ route('request_carts.update', ['request_cart' => $product->id]) }}" method="POST">
                @csrf
                @method('PUT')
                <h5 class="my-3 text-center">Add {{ $product->name }} to your request form?</h5>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success add-to-cart" data-id="{{ $product->id }}">Add to request</button>
        </form>
        </div>
      </div>
    </div>
</div>