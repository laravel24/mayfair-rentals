@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6 col-lg-6 col-md-12 col-sm-12 p-1 d-flex justify-content-center">
            <img src="{{ $product->image }}" alt="{{ $product->name }}" style="height:40em">
            
        </div>
        <div class="col-6 col-lg-6 col-md-12 col-sm-12 p-1">
            <h1>{{ $product->name }}</h1>
            <p><span class="badge 
                @if($product->type->category->name == 1)
                badge-danger badge-c
                @elseif($product->type->category->name == 2)
                badge-warning badge-j
                @else
                badge-info badge-p
                @endif
                ">{{ $product->type->category->name }}</span> <span class="badge badge-secondary">{{ $product->type->name }}</span> 
                {{-- <span class="badge badge-secondary">LABELS</span> --}}
            </p>
            <h3 class="mb-3">
                &#8369; {{ number_format($product->price, 2) }}
            </h3>

            @cannot('isAdmin')
            <div class="my-1">
                <button class="btn btn-lg btn-success my-1 mb-3" data-toggle="modal" data-target="#modal-for-request{{ $product->id }}">Request Item</button> 
            </div>


<!-- Modal for request -->
  <div class="modal fade" id="modal-for-request{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel2">Request {{ $product->name }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
  
              <div class="d-flex">
                  <img src="{{ $product->image }}" class="mr-3" alt="" style="height:8em">

                  <div>
                      <p class="d-flex align-items-center">Product Name: {{ $product->name }}</p>
      
                      <p class="d-flex align-items-center">Product Code: {{ $product->code }}</p>
  
                      <p class="d-flex align-items-center">Base Rent Price: &#8369; {{ $product->price }}</p>
                  </div>
              </div>
  
              <hr>
  
              <form action="{{ route('request_carts.update', ['request_cart' => $product->id]) }}" method="POST">
                  @csrf
                  @method('PUT')
                  <h5 class="my-3 text-center">Add {{ $product->name }} to your request form?</h5>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-success add-to-cart" data-id="{{ $product->id }}">Add Item</button>
        </div>
      </div>
    </div>
  </div>

            @endcannot

            {{-- <div class="input-group my-3 w-75">
                <input type="number" class="form-control" value="1">
                <div class="input-group-append">
                  <span class="input-group-text" type="button">SUBTOTAL: XXXX</span>
                  <button class="btn btn-outline-secondary" type="button">ADD TO CART</button>
                </div>
            </div> --}}

            <p class="m-0">ABOUT THIS PRODUCT</p>
            <hr class="my-1">
            <p>{{ $product->description }} 
                {{-- // A fragrant fantasy, this aromatic green tea blend of TWG Tea's signature 1837 Tea is a voyage, boasting a lofty fruit and flower bouquet that leaves a delicious aftertaste of red fruits and caramel with a light touch of astringency. A delightful tea for any time of the day and perfect paired with desserts. --}}
            </p>

        </div>

        <div class="col-12">
            <hr>
            <h5 class="text-center mb-3">You may also like</h5>

            <div class="d-flex">
                @foreach ($similar_products as $product)
                <div class="col-4">
                    @include('products.partials.product_card')
                </div>
                @endforeach
            </div>
        </div>

        

        {{-- <div class="col-12">
            <hr>
            <h5 class="text-center mb-3">Comments</h5>

            <div class="form-group">
                <label for="exampleFormControlTextarea1">Your comment:</label>
                <textarea class="form-control mb-1" id="exampleFormControlTextarea1" rows="3"></textarea>
                
                <div class="d-flex mb-5">
                    
                    <div class="ml-auto">
                        <button class="btn btn-secondary m-0">Cancel</button>
                        <button class="btn btn-danger m-0">Comment</button>
                    </div>
                    
                </div>

                <div class="d-flex">
                    <div class="pr-1"><img src="https://via.placeholder.com/100" alt=""></div>
                    <div class="p-1">
                        <h5>USERNAME: XXXX</h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima voluptatem, recusandae ducimus dignissimos hic rem. Dolorum dolores iste obcaecati quis, nesciunt excepturi, modi explicabo minus, ex quibusdam in quo ab.</p>
                    </div>
                </div>
                

            </div>
        </div> --}}        

    </div>
</div>
@endsection