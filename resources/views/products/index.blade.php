@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 mb-2">
            @includeWhen($errors->any(), 'partials.error_message')
            @includeWhen(Session::has('success'), 'partials.success_message')

            <div class="d-flex flex-wrap align-items-center">
                <h1 class="mr-auto">Products List</h1>
                @can('isAdmin')
                <p><button class="btn btn-warning btn-lg ml-auto" data-toggle="modal" data-target="#add-product">Add New Product</button></p>
                @endcan
            </div>
            <hr>

            <div class="d-flex align-items-center">
                <h4 class="mr-2 pt-1 pb-0">Sort by: </h4>
                <a href="{{ route('products.index') }}" class="btn btn-outline-dark mr-1">All</a>
                <form action="{{ route('products.index') }}">
                    @foreach ($categories as $category)
                    <button type="submit" class="btn btn-outline-dark" name="category_id" value="{{ $category->id }}" {{ Request::query("category_id") == $category->id ? "Selected" : ""}}>{{ $category->name }}</button>
                    @endforeach
                </form>
            </div>
            
        </div>
    </div>

    <hr>

    <div class="d-flex flex-wrap">
        @foreach ($products as $product)
            <div class="col-4">
                @include('products.partials.product_card')
            </div>
        @endforeach
    </div>
    @can('isAdmin')
    <div class="d-flex justify-content-center mt-5">
        <a href="{{ route('products.deleted') }}" class="btn btn-outline-dark">See deleted products</a>
    </div>
    @endcan
</div>

<!-- modal -->
<div class="modal fade" id="add-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add new product</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label for="name">Product Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter product name">
                </div>

                <div class="form-group">
                    <label for="price">Product Price</label>
                    <input type="text" class="form-control" name="price" placeholder="Enter product price">
                </div>

                <div class="form-group">
                    <label for="image">Product Image</label>
                    <input type="file" class="form-control" name="image">
                </div>

                <div class="form-group">
                    <label for="description">Product Description</label>
                    <textarea class="form-control" name="description" placeholder="Enter product description" rows="3"></textarea>
                </div>

                <div class="form-group">
                    <label for="type_id">Product Type</label>
                    <select class="btn btn-outline-primary mx-1" name="type_id">
                        @foreach ($types as $type)
                        <option class="dropdown-item" value="{{ $type->id }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                {{-- </div>

                <div class="form-group"> --}}
                    <label for="origin_id">Product Origin</label>
                    <select class="btn btn-outline-primary mx-1" name="origin_id">
                        @foreach ($origins as $origin)
                        <option class="dropdown-item" name="origin_id" value="{{ $origin->id }}">{{ $origin->name }}</option>
                        @endforeach
                    </select>
                </div>  
                
                <div class="form-group">
                    <label for="product_status_id">Availability</label>
                    <select class="btn btn-outline-primary mx-1" name="product_status_id">
                        @foreach ($product_statuses as $product_status)
                        <option class="dropdown-item" name="product_status_id" value="{{ $product_status->id }}">{{ $product_status->name }}</option>
                        @endforeach
                    </select>
                </div>

                <hr class="mt-0">
                <div class="col-12 d-flex flex-wrap p-0">
                    @foreach($labels as $label)
                    <div class="input-group mb-3 col-6">
                        <div class="input-group-prepend">
                        <div class="input-group-text">
                            <input type="checkbox" name="label_id[]" value="{{ $label->id }}" aria-label="Checkbox for following text input">
                        </div>
                        </div>
                        <label for="label_id[]" class="form-control">{{ $label->name }}</label>
                    </div>
                    @endforeach
                </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
        </div>
      </div>
    </div>
</div>

@endsection