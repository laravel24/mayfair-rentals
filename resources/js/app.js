require("./bootstrap");

let today = new Date();
let day = today.getDate();
let month = today.getMonth() + 1;
let year = today.getFullYear();

let minBorrowDay = today.getDate() + 1;
let borrowMonth = today.getMonth() + 1;

if (day < 10) {
  minBorrowDay = "0" + minBorrowDay;
}

maxBorrowMonth = borrowMonth + 3;

if (month < 10) {
  borrowMonth = "0" + borrowMonth;
  maxBorrowMonth = "0" + maxBorrowMonth;
}

let minBorrowDate = year + "-" + borrowMonth + "-" + minBorrowDay;
let minReturnDate = year + "-" + borrowMonth + "-" + (minBorrowDay + 1);
let maxReturnDate = year + "-" + maxBorrowMonth + "-" + minBorrowDay;

document.getElementById("borrow_date").setAttribute("min", minBorrowDate);
document.getElementById("return_date").setAttribute("min", minReturnDate);
document.getElementById("return_date").setAttribute("max", maxReturnDate);

///

var addToCartBtns = document.querySelectorAll(".add-to-cart"); // console.log(addToCartBtns);

addToCartBtns.forEach(function (addToCartBtn) {
  // addToCartBtn.addEventListener("click", () => {
  //     console.log(this); //this will log window because ()=>{} has diff scoping
  // });
  addToCartBtn.addEventListener("click", function () {
    console.log(this.dataset.id); //this will log the dataset id

    var id = this.dataset.id; // console.log(document.querySelector("input[data-id='"  id  "']")); same thing below, just longer
    // console.log(document.querySelector(`input[data-id="${id}"]`));
    //here we will get the value instead

    // var quantity = document.querySelector('input[data-id="'.concat(id, '"]'))
    //   .value; // console.log(quantity);

    // $("#element").toast("show");

    // if (!isNaN(quantity) || quantity == "") {
    //   return alert("Quantity should be a number!");
    // }

    var csrfToken = document
      .querySelector('meta[name="csrf-token"]')
      .getAttribute("content"); // console.log(csrfToken);

    var url = "/api/carts/update-cart";
    fetch(url, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "X-CSRF-TOKEN": csrfToken,
      },
      body: JSON.stringify({
        id: id,
        // quantity: quantity, // the id (: id) is from the id above
      }),
    })
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        console.log(data);
        document.querySelector("#cart-count").innerHTML = data.message;
      });
  });
}); // fetch
// you will pass the info on the body
