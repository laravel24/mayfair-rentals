<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequest extends Model
{
    protected $fillable = ['code', 'total', 'borrow_date', 'return_date', 'user_id', 'product_id', 'request_status_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'requested_product')->withPivot('price')->withTrashed()->withTimestamps();
    }

    public function request_status()
    {
        return $this->belongsTo('App\RequestStatus');
    }
}