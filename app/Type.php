<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['name', 'category_id'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function product() {
        return $this->hasMany('App\Product')->withTrashed();
    }
}