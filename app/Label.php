<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
    public function product()
    {
        return $this->belongsToMany('App\Product', 'product_label', 'product_id', 'label_id')->withTimestamps();
    }
}