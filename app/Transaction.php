<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['code', 'address', 'total', 'payment_mode_id', 'user_request_id', 'status_id', 'user_id'];

    public function user_request()
    {
        return $this->belongsTo('App\UserRequest');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}