<?php

namespace App\Http\Controllers;

use App\Type;
use App\Category;
use App\Product;
use App\Origin;
use App\ProductStatus;
use App\Label;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Product $product)
    {
        $this->authorize('viewAny', 'App\Type');
        
        $products = Product::all();
        $categories = Category::all();
        if ($request->query('category_id')) {
            $types = Type::where('category_id', $request->query('category_id'))->get()->sortBy('name');
        } elseif($request->query('search_category')) {
            $types = Type::where('name', 'like', $request->query('search_category') . '%')->orWhere('name', 'like', '%' . $request->query('search_category') . '%')->get()->sortBy('name')->sortBy('category_id');
            
            if ($types->count() == 0) {
                return redirect()->back()->with('empty_search', 'No items match the search.');
            }
                
        } else {
            $types = Type::all()->sortBy('name');
        }

        return view('types.index')->with(['types' => $types, 'categories' => $categories, 'products' => $products]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Type $type, Category $category)
    {
        $this->authorize('create', 'App\Type');
        
        $validatedData = $request->validate([
            'name' => 'required|unique:types,name|string|max:20',
            'category_id' => 'required'
        ]);

        $type = new Type($validatedData);
        $type->save();
        
        return redirect(route('types.index'))->with('success', "Type $type->name successfully created.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type, Product $product)
    {
        $this->authorize('view', 'App\Type');

        $labels = Label::all();
        $products = Product::where("type_id", $type->id)->get();
        $product_statuses = ProductStatus::all();
        $origins = Origin::all();
        $types = Type::all();
        $categories = Category::all();
        return view('types.show')->with(['categories' => $categories, 'products' => $products, 'types' => $types, 'origins' => $origins, 'product_statuses' => $product_statuses, 'labels' => $labels]);
        // $categories = Category::all();
        // $types = Type::all();
        // $origins = Origin::all();
        // return view('types.show')->with(['categories' => $categories, 'similar_products' => $similar_products, 'product' => $product, 'types' => $types, 'origins' => $origins]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', 'App\Type');

        $validatedData = $request->validate([
            'name' => 'required|string|max:20',
            'category_id' => 'required'
        ]);

        $type = Type::find($id);

        $type->update($validatedData);

        return redirect(route('types.index'))->with('success', "Type $type->name successfully updated.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        $this->authorize('delete', 'App\Type');
        
        $type->delete();
        return redirect(route('types.index'))->with('success', "Type $type->name  successfully deleted.");
    }

    public function deletedTypes() 
    {
        $this->authorize('viewAny', 'App\Type');

        $products = Product::all();
        $categories = Category::all();
        $types = Type::onlyTrashed()->get();
        return view('types.deleted')->with(['categories' => $categories, 'products' => $products, 'types' => $types]);
    }

    public function restore($type, Type $typeres)
    {
        $this->authorize('viewAny', 'App\Type');
        $typeres = Type::withTrashed()->where('id', $type)->get();
        $typeres_name = $typeres[0]->name;
        Type::withTrashed()->find($type)->restore();
        return redirect('/types')->with('success', "Type $typeres_name successfully restored.");
    }
}