<?php

namespace App\Http\Controllers;

use App\UserRequest;
use App\RequestStatus;
use Illuminate\Http\Request;
use App\Category;
use Auth;
use Str;
use Session;
use App\Product;

class UserRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', 'App\UserRequest');
        
        $products = Product::withTrashed();
        $request_statuses = RequestStatus::all();
        // $user_requests = UserRequest::all()->sortByDesc('created_at');
        $categories = Category::all();

        if(Auth::user()->role_id == 1) {
            $user_requests = UserRequest::all()->sortByDesc('created_at');
            
            if ($request->query('request_status_id')) {
                $user_requests = UserRequest::all()->where('request_status_id', $request->query('request_status_id'))->sortByDesc('created_at');
            }

        } else {
            // dd(Auth::user()->id);
            $user_requests = UserRequest::where("user_id", Auth::user()->id)->get()->sortByDesc('created_at');

            if ($request->query('request_status_id')) {
                $user_requests = UserRequest::where('request_status_id', $request->query('request_status_id'))->where("user_id", Auth::user()->id)->get()->sortByDesc('created_at');
            }
        }

        return view('user_requests.index')->with(['categories' => $categories, 'user_requests' => $user_requests, 'request_statuses' => $request_statuses, 'products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
        // $categories = Category::all();
        // return view('requests.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_statuses = RequestStatus::all();
        $user_requests = UserRequest::all();
        $categories = Category::all();
        if (Auth::user() == null) {
            return view('auth/login')->with('categories', $categories);
        }

        $user_id = Auth::user()->id;
        $code = $user_id . strtoupper(Str::random(9));
        $borrow_date = $request->borrow_date;
        $return_date = $request->return_date;
    
        $user_request = new UserRequest([
            'code' => $code,
            'borrow_date' => $borrow_date,
            'return_date' => $return_date,
            'total' => 0,
            'user_id' => $user_id,
            'request_status_id' => 1,
        ]);

        $user_request->save();

        //------ requested product below

        $cart_ids = array_keys(Session::get('request_cart'));
        $products = Product::find($cart_ids);
        $subtotal = 0;
        $total = 0;

        foreach ($products as $product) {
            foreach (Session::get('request_cart') as $id => $quantity) {
                if ($id == $product->id) {
                    $subtotal += $product->price;
                    $user_request->products()->attach($product->id, ['price' => $product->price]);
                }
            }
        }
        
        $days = date_diff(date_create($borrow_date), date_create($return_date))->format('%a');
        $raw_total = $subtotal * $days;
        $total = $raw_total + 50;

        $user_request->total = $total;
        $user_request->save();

        return redirect(route('user_requests.index'))->with(['categories'=> $categories, 'user_requests' => $user_requests, 'request_statuses' => $request_statuses,  'message' => "Request successfully submitted."]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserRequest  $userRequest
     * @return \Illuminate\Http\Response
     */
    public function show(UserRequest $userRequest)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserRequest  $userRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, UserRequest $user_request)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserRequest  $userRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserRequest $user_request)
    {
        $this->authorize('update', 'App\UserRequest');

        $validatedData = $request->validate([
            'request_status_id' => 'required|numeric'
        ]);
        $user_request->update($validatedData);

        // return back();
        return redirect(route('user_requests.index'))->with('success', "Status of request with code $user_request->code successfully edited.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserRequest  $userRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserRequest $userRequest)
    {
        abort(404);
    }
}