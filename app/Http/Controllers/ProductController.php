<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Type;
use App\Origin;
use App\Label;
use App\ProductStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Str;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', 'App\Product');
        // $products = Product::with('type.category')->get();
        $products = Product::all();
        $types = Type::all();
        $origins = Origin::all();
        $categories = Category::all();
        $labels = Label::all();
        $product_statuses = ProductStatus::all();

        if ($request->query('category_id')) {
            $products = Product::with('type.category')->select('products.id', 'products.name', 'products.price', 'products.image', 'products.description', 'products.type_id', 'products.origin_id', 'types.category_id')->join('types', 'types.id', '=', 'products.type_id')->where("category_id", $request->query('category_id'))->get();
        }
    
        return view('products.index', ['categories' => $categories, 'products' => $products, 'types' => $types, 'origins' => $origins, 'product_statuses' => $product_statuses, 'labels' => $labels]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Type $type, Product $product)
    {   
        $this->authorize('create', 'App\Product');

        $code = "MF-" . strtoupper(Str::random(10));
        $validatedData = $request->validate([
            'name' => 'required|string',
            'code' => 'unique',
            'price' => 'required|numeric',
            'image' => 'required|image|max:5000',
            'description' => 'required|string',
            'type_id' => 'required|numeric',
            'origin_id' => 'required|numeric',
            'product_status_id' => 'required|numeric',
        ]);

        // dd($validatedData);

        $image_path = $request->file('image')->store('public/products', 's3');

        $product = new Product([
            'name' => $request->name,
            'code' => $code,
            'price' => $request->price,
            'description' => $request->description,
            'type_id' => $request->type_id,
            'origin_id' => $request->origin_id,
            'product_status_id' => $request->product_status_id,
            // 'image' => $request->image
            'image' => Storage::disk('s3')->url($image_path)
        ]);

        $product->image = Storage::url($image_path);
        
        $product->save();

        //========== save to pivot
        $label_ids = $request->input('label_id');
        
        if ($label_ids !== null) {
            $selected_labels = Label::find($label_ids);
            
            foreach ($selected_labels as $selected_label) {
                $product->label()->attach($selected_label->id);
            }
        }
        
        return redirect(route('products.index'))->with('success', "Product $type->name successfully created.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $this->authorize('view', 'App\Product');
        
        $labels = Label::all();
        $product_statuses = ProductStatus::all();
        $similar_products = Product::where('type_id', $product->type_id)->where('id', '!=', $product->id)->take(3)->get();
        $categories = Category::all();
        $types = Type::all();
        $origins = Origin::all();
        return view('products.show')->with(['categories' => $categories, 'similar_products' => $similar_products, 'product' => $product, 'types' => $types, 'origins' => $origins, 'product_statuses' => $product_statuses, 'labels' => $labels]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {

        $this->authorize('update', 'App\Product');
        
        $validatedData = $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric',
            'image' => 'image|max:5000',
            'description' => 'required|string',
            'type_id' => 'required|numeric',
            'origin_id' => 'required|numeric',
            'product_status_id' => 'required|numeric'
        ]);

        $product->update($validatedData);

        if ($request->hasFile('image')) {
            $image_path = $request->file('image')->store('public/products');
            $product->image = Storage::url($image_path);
        }

        $product->save();

        //====== update pivot
            
        $label_ids = $request->input('label_id');
        if ($label_ids !== null) {    
            $selected_labels = Label::find($label_ids);

            if(DB::table('product_label')->where('product_id', $product->id)->exists()){
                foreach ($selected_labels as $selected_label) {
                    $label_id_array[$selected_label->id] = $selected_label['id'];
                }

                $product->label()->sync($label_id_array);  
            } else {
                foreach ($selected_labels as $selected_label) {
                    $product->label()->attach($selected_label->id);
                }
            }  
        }

        return redirect(route('products.index'))->with('success', "Product $product->name edited successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('delete', 'App\Product');

        $product->delete();
        return redirect(route('products.index'))->with('success', "Product $product->name successfully deleted");
    }

    public function deleted()
    {
        $this->authorize('viewDeleted', 'App\Product');

        $products = Product::onlyTrashed()->get();
        $categories = Category::all();
        $types = Type::all();
        $origins = Origin::all();
        $labels = Label::all();
        $product_statuses = ProductStatus::all();
        
        return view('products.deleted')->with(['categories' => $categories, 'products' => $products, 'types' => $types, 'origins' => $origins, 'product_statuses' => $product_statuses, 'labels' => $labels]);
    }

    public function restore($product, Product $productres)
    {
        $this->authorize('restore', 'App\Product');

        $productres = Product::withTrashed()->where('id', $product)->get();
        $productres_name = $productres[0]->name;
        Product::withTrashed()->find($product)->restore();
        return redirect('/products')->with('success', "Type $productres_name successfully restored.");
    }

    public function by_category(Request $request, Category $category, Type $type)
    {
        $this->authorize('viewAny', 'App\Product');
        
        $products = Product::with('type.category')->select('products.id', 'products.name', 'products.price', 'products.image', 'products.description', 'products.type_id', 'products.origin_id', 'types.category_id')->join('types', 'types.id', '=', 'products.type_id')->where("category_id", $category->id)->get();

        $types = Type::where('category_id', $category->id)->get();
        $origins = Origin::all();
        $categories = Category::all();
        $labels = Label::all();
        $product_statuses = ProductStatus::all();

        if($request->query('type_id')){
            $products = Product::with('type.category')->select('products.id', 'products.name', 'products.price', 'products.image', 'products.description', 'products.type_id', 'products.origin_id', 'types.category_id')->join('types', 'types.id', '=', 'products.type_id')->where("type_id", $request->query('type_id'))->get();
        }

        if($request->query('origin_id')){
            $products = Product::with('type.category')->select('products.id', 'products.name', 'products.price', 'products.image', 'products.description', 'products.type_id', 'products.origin_id', 'types.category_id')->join('types', 'types.id', '=', 'products.type_id')->where('category_id', $category->id)->where('origin_id', $request->query('origin_id'))->get();
        }

        if($request->query('label_id')){
            // dd($products[1]->label);
            // dd(Label::where('id', $request->query())->get());
            // $products = Product::where('')
            $product_labels = DB::table('product_label')->where('label_id', $request->query('label_id'))->get();

            $label_id_array = [];

            foreach($product_labels as $product_label) {
                $label_id_array[$product_label->id] = $product_label->product_id;
            }
            
            // $products = Product::all();

            $products = Product::with('type.category')->select('products.id', 'products.name', 'products.price', 'products.image', 'products.description', 'products.type_id', 'products.origin_id', 'types.category_id')->join('types', 'types.id', '=', 'products.type_id')->where('category_id', $category->id)->whereIn('products.id', $label_id_array)->get();
        }
    
        return view('products.by_category', ['category' => $category, 'categories' => $categories, 'products' => $products, 'types' => $types, 'origins' => $origins, 'product_statuses' => $product_statuses, 'labels' => $labels]);
    }
}