<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Session;
use Auth;

class RequestCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('form', 'App\UserRequest');

        if(Auth::check()) {
            if(Auth::user()->role_id == 1) {
                abort(403, 'This action is unauthorized.');
            }
        }

        $subtotal = 0;
        
        $categories = Category::all();
        if (Session::has('request_cart')) {
            $product_ids = array_keys(Session::get('request_cart'));
            $products = Product::find($product_ids);

            foreach ($products as $product) {
                $subtotal += $product->price;
            }

        } else {
            $products = [];
            $subtotal = 0;
        }
        
        
        return view('request_carts.index')->with(['categories' => $categories, 'products' => $products, 'subtotal' => $subtotal]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $request_cart)
    {
        $this->authorize('form', 'App\UserRequest');
        
        if(Auth::user() !== null) {
            if(Auth::user()->role_id == 1) {
                abort(403, 'This action is unauthorized.');
            }
        }

        $request->session()->put("request_cart.$request_cart");

        // return redirect(route('request_carts.index'));
        return redirect(route('products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('form', 'App\UserRequest');
        
        if(Auth::check()) {
            if(Auth::user()->role_id == 1) {
                abort(403, 'This action is unauthorized.');
            }
        }
        
        Session::forget("request_cart.$id");

        if (count(Session::get('request_cart')) === 0) {
            $this->clear();

            return redirect(route('request_carts.index'))->with('empty_form', "No items in request form.");
        }
        
        return redirect(route('request_carts.index'))->with('success', "Item successfully removed from request form");
    }

    public function clear()
    {   
        $this->authorize('form', 'App\UserRequest');
        
        if(Auth::check()) {
            if(Auth::user()->role_id == 1) {
                abort(403, 'This action is unauthorized.');
            }
        }
        
        Session::forget('request_cart');
        
        return redirect(route('request_carts.index'));
    }

    public function updateCart(Request $request)
    {

        // get data from fetch
        // "true" converts the data into an associative array

        $fetchedData = json_decode($request->getContent(), true);
        // now you can select id from fetchedData
        $id = $fetchedData['id'];
        $quantity = $fetchedData['quantity'];
        // return response()->json(["message" => [$id, $quantity]]);

        //
        $request->session()->put("request_cart.$id", $quantity);

        return response()->json(['message' => count(Session::get('request_cart'))]);

        // $id = $request->id; -- this will show null unlike the one above bec you really need to decode the string as json
        // return response()->json(["message" => $id]);
        // return "text";
    }
}