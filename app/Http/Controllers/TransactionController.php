<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Category;
use App\UserRequest;
use App\RequestStatus;
use App\Status;
use Illuminate\Http\Request;
use Auth;
use Str;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', 'App\Transaction');
        
        $categories = Category::all();
        $statuses = Status::all();

        if (Auth::user()->role_id == 1) {
            $transactions = Transaction::all()->sortByDesc('created_at');
            
        // if ($request->query('request_status_id')) {
            //     $user_requests = UserRequest::all()->where('request_status_id', $request->query('request_status_id'))->sortByDesc('created_at');
            // }
        } else {
            // dd(Auth::user()->id);
            $transactions = Transaction::where("user_id", Auth::user()->id)->get()->sortByDesc('created_at');

            // if ($request->query('request_status_id')) {
            //     $user_requests = UserRequest::where('request_status_id', $request->query('request_status_id'))->where("user_id", Auth::user()->id)->get()->sortByDesc('created_at');
            // }
        }

        return view('transactions.index')->with(['categories' => $categories, 'transactions' => $transactions, 'statuses' => $statuses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $user_request)
    {
        $this->authorize('create', 'App\Transaction');
        
        $request_statuses = RequestStatus::all();
        
        $user_request_query = UserRequest::where('id', $user_request)->get();
        $user_request = $user_request_query[0];
        // dd($user_request);
        $categories = Category::all();
        return view('transactions.create')->with(['categories' => $categories, 'user_request' => $user_request, 'request_statuses' => $request_statuses]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $user_request)
    {
        $this->authorize('create', 'App\Transaction');

        // $categories = Category::all();
        // $user_request = UserRequest::where('id', $user_request)->get();
        
        if ($request->address == null) {
            abort(403, "Forbidden action");
        }

        $categories = Category::all();
        $user_request = UserRequest::where('id', $user_request)->get();
        
        $validatedData = $request->validate([
            'address' => 'required|string',
        ]);
    
        $address = $request->address;
        $code = "T" . Auth::user()->id . strtoupper(Str::random(8));
        $user_id = Auth::user()->id;


        $transaction = new Transaction([
            'code' => $code,
            'address' => $address,
            'total' => $user_request[0]->total,
            'payment_mode_id' => 1,
            'user_request_id' => $user_request[0]->id,
            'status_id' => 1,
            'user_id' => $user_id,

        ]);

        $transaction->save();

        return redirect(route('transactions.index'))->with(['categories' => $categories, 'user_request' => $user_request]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $this->authorize('update', 'App\Transaction');

        $validatedData = $request->validate([
            'status_id' => 'required|numeric'
        ]);
        
        $transaction->update($validatedData);

        return redirect(route('transactions.index'))->with('success', "Status of transaction with code $transaction->code successfully edited.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        abort(404);
    }
}