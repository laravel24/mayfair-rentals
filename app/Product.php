<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['name', 'code' ,'price', 'image', 'description', 'type_id', 'origin_id', 'product_status_id'];

    public function type()
    {
        return $this->belongsTo('App\Type')->withTrashed();
    }

    public function category()
    {
        return $this->hasOneThrough('Category', 'Type');
    }

    public function product_status()
    {
        return $this->belongsTo('App\ProductStatus');
    }

    public function user_request()
    {
        return $this->hasMany('App\UserRequest')->withTrashed();
    }

    public function label()
    {
        return $this->belongsToMany('App\Label', 'product_label', 'product_id', 'label_id')->withTimestamps();
    }
}