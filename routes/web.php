<?php

use Illuminate\Support\Facades\Route;

use App\Category;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/types/deleted', 'TypeController@deletedTypes')->name('types.deleted');
Route::put('/types/{type}/restore', 'TypeController@restore')->name('types.restore');

Route::get('/products/deleted', 'ProductController@deleted')->name('products.deleted');
Route::put('/products/{product}/restore', 'ProductController@restore')->name('products.restore');

Route::put('/transactions/{user_request}/create', 'TransactionController@create')->name('transactions.create');
Route::post('/transactions/{user_request}/store', 'TransactionController@store')->name('transactions.store');
Route::get('/transactions', 'TransactionController@index')->name('transactions.index');
Route::put('/transactions/{transaction}/update', 'TransactionController@update')->name('transactions.update');

Route::get('/products/{category}/by_category', 'ProductController@by_category')->name('products.by_category');

Route::delete('/request_carts/clear', 'RequestCartController@clear')->name('request_carts.clear');

Route::resources([
    'categories' => 'CategoryController',
    'types' => 'TypeController',
    'products' => 'ProductController',
    'user_requests' => 'UserRequestController',
    'request_carts' => 'RequestCartController',
]);